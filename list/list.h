#ifndef __LIST_H__
#define __LIST_H__

namespace list {
    class List;
}

#include <algorithm>
using namespace std;

#include "../pathing/graph.h"

using namespace pathing;

namespace list {
    class List {
        List *next, *prev;
        void rev_prev();
        void rev_next();

        public:
        double first, second;
        List(Point p);
        List *add_behind(Point p);
        List *add_before(Point p);
        bool is_first();
        bool is_last();
        void reverse();
        void concat(List* l);
        List* travel_first();
        List* travel_last();
    };
}

#endif
