#include "list.h"
#include "../pathing/graph.h"

namespace list {
    List :: List(Point p) {
        next = prev= NULL;
        first = p.x;
        second = p.y;
    }

    List* List :: add_behind(Point p) {
        List* temp = next;
        next = new List(p);
        next -> next = temp;
        if (temp != NULL) {
            temp -> prev = next;
        }
        next -> prev = this;
        return next;
    }

    List* List :: add_before(Point p) {
        List* temp = prev;
        prev = new List(p);
        prev -> prev = temp;
        if (temp != NULL) {
            temp -> next = prev;
        }
        prev -> next = this;
        return prev;
    }

    bool List :: is_first() {
        return prev == NULL;
    }

    bool List :: is_last() {
        return next == NULL;
    }

    void List :: reverse() {
        if (next != NULL) {
            next -> rev_next();
        }
        if (prev != NULL) {
            prev -> rev_prev();
        }
        swap(next, prev);
    }

    void List :: rev_next() {
        if (next != NULL) {
            next -> rev_next();
        }
        swap(next, prev);
    }

    void List :: rev_prev() {
        fprintf(stderr, "rev_prev %lf %lf\n", first, second);
        if (prev != NULL) {
            prev -> rev_prev();
        }
        swap(next, prev);
    }

    void List :: concat(List* l) {
        if (next != NULL || l -> prev != NULL) {
            printf("bad concat %s!\n", next != NULL ? "next" : "prev");
        }
        next = l;
        l -> prev = this;
    }

    List* List :: travel_first() {
        if (prev == NULL) return this;
        return prev -> travel_first();
    }

    List* List :: travel_last() {
        if (next == NULL) return this;
        return next -> travel_last();
    }
}
