#ifndef _viz_h
#define _viz_h

class Viz;

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "../map.h"

class Viz
{
    map::Map *M;

    public:
    Viz(map::Map *M_) : M(M_), dot(NULL), scale(NULL), robot(NULL),
    screen(NULL), scale_msg(NULL), font(NULL),
    textColor({255, 255, 255}), SIZE_X(480), SIZE_Y(360) {}
    SDL_Surface* dot;
    SDL_Surface* scale;
    SDL_Surface* robot;
    SDL_Surface* screen;
    SDL_Surface* scale_msg;
    TTF_Font* font;
    SDL_Color textColor;
    int start();
    int stop();
    int render(int a);
    void set_scale(char* scale, SDL_Surface* destination );


    int SIZE_X;
    int SIZE_Y;


};



#endif
