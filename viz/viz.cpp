//Include SDL functions and datatypes
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "viz.h"
#include <iostream>
#include "../map.h"

SDL_Surface *load_image( std::string filename ) 
{
    //Temporary storage for the image that's loaded
    SDL_Surface* loadedImage = NULL;
    
    //The optimized image that will be used
    SDL_Surface* optimizedImage = NULL;
   //Load the image
    loadedImage = IMG_Load( filename.c_str() );
    //If nothing went wrong in loading the image
    if( loadedImage != NULL )
    {
        //Create an optimized image
        optimizedImage = SDL_DisplayFormat( loadedImage );
        
        //Free the old image
        SDL_FreeSurface( loadedImage );
    }
    //Return the optimized image
    return optimizedImage;
}
void apply_surface( int x, int y, SDL_Surface* source, SDL_Surface* destination )
{
    //Make a temporary rectangle to hold the offsets
    SDL_Rect offset;
    
    //Give the offsets to the rectangle
    offset.x = x;
    offset.y = y;
        //Blit the surface
    SDL_BlitSurface( source, NULL, destination, &offset );
}
void Viz::set_scale(char* scale_text, SDL_Surface* destination )
{
    //Make a temporary rectangle to hold the offsets
    SDL_Rect offset;
    
    //Give the offsets to the rectangle
    offset.x = 30;
    offset.y = SIZE_Y-30;
    //Blit the surface
    SDL_BlitSurface( scale, NULL, destination, &offset );

    offset.x = 30;
    offset.y = SIZE_Y-50;  
    scale_msg = TTF_RenderText_Solid( font, scale_text, textColor );
    SDL_BlitSurface( scale_msg, NULL, destination, &offset );
}
int Viz::start()
{
    if( TTF_Init() == -1 )
    {
       //panika!  
    }
//Start SDL
    SDL_Init( SDL_INIT_EVERYTHING );

    //Set up screen
    screen = SDL_SetVideoMode( SIZE_X, SIZE_Y, 32, SDL_SWSURFACE );
    SDL_WM_SetCaption( "Robot Client", NULL );
    //Load image

    dot = load_image( "viz/dot.png" );
    scale = load_image( "viz/scale.png" );
    font = TTF_OpenFont( "viz/DejaVuSans.ttf", 14 );


    return 0;
}
int Viz::render(int a)
{


    SDL_FillRect( screen, &screen->clip_rect, SDL_MapRGB( screen->format, 0x00, 0x00, 0x00 ) );

    vector<pair<int,int> > points = M->get_positions(SIZE_X, SIZE_Y);
    int scale = points[0].first;
    int scalebar_length = 50;
    int num_points = points.size();
    printf("rendering %d points\n", num_points);
    char str_scale[50];
    sprintf(str_scale,"%d cm", scale*scalebar_length);
    set_scale(str_scale , screen);
    for (int i = 1; i < num_points; i++) {
        int x = points[i].first;
        int y = points[i].second;
        apply_surface( x, y, dot, screen );
    }


    //Update Screen
    SDL_Flip( screen );

    //Pause
    SDL_Delay( 2000 );


    return a;
}
int Viz::stop()
{
    //Free the loaded image
    SDL_FreeSurface( dot );
    SDL_Quit();
    return 0;

}
