#include <iostream>
#include <stdio.h>
#include <stdlib.h>	
#include <string.h>
#include "libs/clienttcp.h"
#include "libs/robotState.h"
#include "viz/viz.h"
#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "map.h"


using namespace std;

int main(int argc, char *argv[])
{
    Client client;
    char buffer[256];
    int portno;
    char *hostname;


    if (argc < 3) {
        fprintf(stderr,"usage %s hostname port (optional mapname)\n", argv[0]);
        exit(0);
    }


    //connecting to host
    portno = atoi(argv[2]);
    hostname = argv[1];

    client.conn(hostname,portno);
    if (argc > 3)
    {
        client.load(argv[3]);
    }


    map::Map M(client);
    M.initMap();

    while (true) {
        M.explore();
    }


    //  //loop
    // printf("Please enter the message: ");
    //  bzero(buffer,256);
    //  fgets(buffer,255,stdin);
    //  while(strcmp (buffer,"quit\n") !=0)
    //  {
    //  	client.settask(buffer);
    //   printf("Please enter the message: ");
    //   bzero(buffer,256);
    //   fgets(buffer,255,stdin);
    // }


    //disconnect before exit
    client.disconnect();
    return 0;
}
