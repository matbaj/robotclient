#include <cstdio>
#include <vector>
#include <cmath>
using namespace std;

#include "graph.h"
#include "../libs/robotState.h"

namespace pathing {

    Point random_point(int min_x, int max_x, int min_y, int max_y) {
        double rand_x = uniformRandom(), rand_y = uniformRandom();
        double x = min_x + rand_x * (max_x-min_x);
        double y = min_y + rand_y * (max_y-min_y);
        return Point(x,y);
    }

    bool Point :: operator<(Point b) {
        if (fabs(x-b.x) > 1e-9) {
            return x < b.x;
        }
        return y < b.y;
    }

    void Graph :: relax() {
        for (int i = 0; i < num_nodes; i++) {
            for (int j = 0; j < i; j++) {
                if (P->can_travel(node_list[i], node_list[j])) {
                    structure[i][j] = structure[j][i] = 1;
                }
            }
        }
        calc_distances();
    }

    void Graph :: calc_distances() {
        for (int k = 0; k < num_nodes; k++) {
            for (int i = 0; i < num_nodes; i++) {
                for (int j = 0; j < num_nodes; j++) {
                    if (structure[i][k] + structure[k][j] < structure[i][j]) {
                        structure[i][j] = structure[i][k] + structure[k][j];
                    }
                }
            }
        }
    }

    double Point :: dist(Point p) {
        return hypot(p.x-x, p.y-y);
    }

    void Graph :: add_node(Point current, int previous) {
        for (int i = 0; i < num_nodes; i++) {
            structure[i].push_back(num_nodes*2);
        }
        node_list.push_back(current);
        num_nodes++;
        structure.push_back(vector<int>(num_nodes, num_nodes*2));
        structure[num_nodes-1][previous] = 1;
        structure[previous][num_nodes-1] = 1;
        structure[num_nodes-1][num_nodes-1] = 0;
        calc_distances();
    }

    vector<Point> Graph :: get_path(int a, int b) {
        if (a == b) {
            return vector<Point>(1,node_list[a]);
        }
        int dist = structure[a][b];
        for (int i = 0; i < num_nodes; i++) {
            if (structure[a][i] == dist-1 && structure[i][b] == 1) {
                vector<Point> res = get_path(a, i);
                res.push_back(node_list[b]);
                return res;
            }
        }
        return vector<Point>();
    }

    vector<Point> Graph :: get_points() {
        return node_list;
    }
    
    // written by:
    // dummy
    double Graph :: get_interesting_score(Point p){
		int k = node_list.size();
		int act_node = 0;
		int score = node_list[act_node].dist(p);
		for(int i = 0; i < k; i++){
			if(node_list[i].dist(p) < score){
				act_node = i;
				score = node_list[act_node].dist(p);
			}
		}
		return sqrt(score) + 6.66/(1+get_path(current_node, act_node).size()); //magic, do not touch, glows at night
	}


    Graph :: Graph(Point p, map::Map *M_, Pathing *P_) : M(M_), P(P_) {
        num_nodes = 1;
        current_node = 0;
        structure.resize(1,vector<int>(1,0));
        node_list.push_back(p);
    }

    bool Graph :: inside(Point p) {
        for (int i = 0; i+1 < num_nodes; i++) {
            if (P->can_travel(p, node_list[i])) {
                return true;
            }
        }
        return false;
    }
}
