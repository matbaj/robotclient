#include <cstdio>
#include <vector>
#include <math.h>       /* fabs */
using namespace std;

#include "pathing.h"
#include "graph.h"
#include "../map.h"
#include "../libs/robotState.h"
#include "../libs/clienttcp.h"

namespace pathing {

    const int MISS_MAX = 10;
    const double ROBOT_RADIUS = 20;
    const double MEASUREMENT_RANGE = 200;

    bool Pathing :: can_travel(Point a, Point b) {
        if (a.dist(b) > MEASUREMENT_RANGE) {
            return false;
        }
        return !M->checkCollisionRectangle(a, b, ROBOT_RADIUS);
    }


    Pathing :: Pathing(map::Map *M_) : M(M_), G(NULL) {}

    bool Pathing :: intersect(Point p, vector<Point> circles) {
        for (int i = 0; i < int(circles.size()); i++) {
            if (p.dist(circles[i]) < ROBOT_RADIUS) {
                return true;
            }
        }
        return false;
    }

    pair<pair<double,double>,pair<double,double> > Pathing :: get_limits(vector<Point> v) {
        double min_x = 1e9, max_x = -1e9, min_y = 1e9, max_y = -1e9;
        for (int i = 0; i < int(v.size()); i++) {
            min_x = min(min_x, v[i].x-MEASUREMENT_RANGE);
            max_x = max(max_x, v[i].x+MEASUREMENT_RANGE);
            min_y = min(min_y, v[i].y-MEASUREMENT_RANGE);
            max_y = max(max_y, v[i].y+MEASUREMENT_RANGE);
        }
        return make_pair(make_pair(min_x, max_x), make_pair(min_y, max_y));
    }

    vector<Point> Pathing :: cover_empty() {
        vector<Point> circles = G->get_points();
        int miss = 0;
        pair<pair<double,double>, pair<double,double> > limits;
        limits = get_limits(circles);
        while (miss < MISS_MAX) {
            Point next = random_point(limits.first.first, limits.first.second, limits.second.first, limits.second.second);
            if (M->checkCollisionCircle(next, ROBOT_RADIUS)) continue;
            if (intersect(next, circles)) {
                miss++;
                continue;
            }
            miss = 0;
            circles.push_back(next);
        }
        return circles;
    }

    vector<Point> Pathing :: get_path_to_next() {
        vector<Point> circles = cover_empty();
        int num_points = G->num_nodes;
        printf("got %d circles, %d initial\n", circles.size(), num_points);
        int position = 0;
        double best_interesting = 0;
        int best_node = 0;
        for (int i = num_points; i < int(circles.size()); i++) {
            int src = -1;
            int dst = 1e9;
            for (int j = 0; j < num_points; j++) {
                if (can_travel(circles[j], circles[i])) {
                    double cur_dist = circles[j].dist(circles[i]);
                    if (cur_dist < dst) {
                        dst = cur_dist;
                        src = j;
                    }
                }
            }
            if (src != -1) {
                double interesting = G->get_interesting_score(circles[i]);
                if (interesting > best_interesting) {
                    best_interesting = interesting;
                    position = i;
                    best_node = src;
                }
            }
        }
        printf("position = %d, best_node = %d, best_interesting = %lf\n", position, best_node, best_interesting);
        printf("start point: %lf %lf, dest point: %lf %lf\n", circles[best_node].x, circles[best_node].y, circles[position].x, circles[position].y);
        printf("dest point: %lf %lf\n", circles[position].x, circles[position].y);
        if (position == 0) {
            // mamy cala mape?
            // i co dalej?
        }
        vector<Point> path = G->get_path(G->current_node, best_node);
        G->add_node(circles[position], best_node);
        path.push_back(G->node_list.back());
        printf("path of %d nodes\n", path.size());
        for (int i = 0; i < path.size(); i++) {
            printf("%lf %lf\n", path[i].x, path[i].y);
        }
        G->current_node = G->num_nodes-1;
        return path;
    }

    void Pathing :: set_graph(Graph *G_) {
        G = G_;
    }

    void Pathing :: seek_and_destroy(RobotState &rstate, Client &robot)
    {
        vector<Point> points_list = get_path_to_next();

        for(int i = 0;i<int(points_list.size())-1;i++)
        {   
            //pozycja points_list[i]
            //cel points_list[i+1]

            double x = points_list[i+1].x-rstate.x;
            double y = points_list[i+1].y-rstate.y;
            printf("move %lf %lf\n", x, y);
            double alpha;
            alpha = y/x;
            
            double angle = atan(alpha) * 180 / M_PI;
            printf("angle before = %lf\n", angle);
            double global;
            //from 0
            if (x>0)
            {
                if (y>0)  global = angle; //x>0 y>0   
                else  global = 360+angle; //x>0 y<=0
            } 
            else
            {    
                if (y>0)  global = 180+angle; //x<0 y>0
                else  global = 180+angle; //x<0 y<=0
            }
            int new_alpha = (int)(360.5+global-rstate.a)%360;

            printf("global = %lf\n", global);


            if (new_alpha>180)
            {
                //nie krec sie jak idiota tylko obroc sie w druga strone
                new_alpha = new_alpha-360;
                robot.rotate(-new_alpha);

            } else {
                robot.rotate(-new_alpha);
            }
            //zapis globalnego obrotu
            rstate.a = global;
            // jedz tam!
            int distance = (int)hypot(x,y);
            robot.go_forward(distance);

            rstate.x = points_list[i+1].x;
            rstate.y = points_list[i+1].y;

            if (i % 3 == 2) {
                M -> update_location();
            }

        }

    }
}
