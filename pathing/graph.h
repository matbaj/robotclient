#ifndef __GRAPH_H__
#define __GRAPH_H__

namespace pathing {
    class Point;
    class Graph;
}

#include <cstdio>
#include <vector>
using namespace std;

#include "../map.h"
#include "pathing.h"

namespace pathing {

    class Point {
        public:
            double x,y;
            Point (double x_=0, double y_=0) : x(x_), y(y_) {}
            Point (pair<double,double> p) : x(p.first), y(p.second) {}
            double dist(Point p);
            bool operator<(Point b);
    };

    // losuje punkt z pewnego prostokata
    Point random_point(int min_x, int max_x, int min_y, int max_y);

    class Graph {
        map::Map *M;
        Pathing *P;
        public:
            // liczba wierzcholkow
            int num_nodes;
            // macierz odleglosci miedzy wierzcholkami
            vector<vector<int> > structure;
            // lista wierzcholkow
            vector<Point> node_list;
            // tu gdzie jest robot
            int current_node;
            // dodaje nowy wierzcholek
            // jako sasiada pewnego wyroznionego
            void add_node(Point current, int previous);
            // szuka nowych krawedzi
            void relax();
            // liczy odleglosci mieszy wszystkimi wierzcholkami
            void calc_distances();
            // zwraca najkrotsza sciezke miedzy a i b
            vector<Point> get_path(int a, int b);
            // zwraca liste wierzcholkow
            vector<Point> get_points();
			//zwraca jakosc punktu w rozumieniu destynacji robota
			double get_interesting_score(Point p);
            // czy punkt jest wewnatrz intseresujacego obszaru
            bool inside(Point p);
            Graph(Point p, map::Map *M_, Pathing *P_);
    };
}

#endif
