#ifndef __PATHING_H__
#define __PATHING_H__

namespace pathing {
    class Pathing;
}

#include "graph.h"
#include "../map.h"
#include "../libs/clienttcp.h"

namespace pathing {

    class Pathing {
        map::Map *M;
        Graph *G;
        // wymysla dokad pojechac
        // i zwraca liste kolejnych przystankow po drodze
        vector<Point> get_path_to_next();
        bool intersect(Point p, vector<Point> circles);
        pair<pair<double,double>, pair<double,double> > get_limits(vector<Point> v);
        vector<Point> cover_empty();
        public:
        void set_graph(Graph *G_);
        bool can_travel(Point a, Point b);
        void seek_and_destroy(RobotState &rstate, Client &robot);
        Pathing(map::Map *M_);
    };
}

#endif
