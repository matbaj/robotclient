all: main 

main: main.o clienttcp.o map.o pathing.o graph.o MC.o robotState.o list.o viz.o
	g++ -o main main.o clienttcp.o map.o pathing.o graph.o MC.o robotState.o list.o viz.o -lSDL -lSDL_image -lSDL_ttf

main.o: main.cpp
	g++ -Wall -c main.cpp

MC.o: libs/MC.cpp libs/MC.h
	g++ -Wall -c libs/MC.cpp

robotState.o: libs/robotState.cpp libs/robotState.h
	g++ -Wall -c libs/robotState.cpp

clienttcp.o: libs/clienttcp.cpp libs/clienttcp.h
	g++ -Wall -c libs/clienttcp.cpp

map.o: map.cpp map.h
	g++ -Wall -c map.cpp -ggdb

pathing.o: pathing/pathing.cpp pathing/pathing.h
	g++ -Wall -c pathing/pathing.cpp

graph.o: pathing/graph.cpp pathing/graph.h
	g++ -Wall -c pathing/graph.cpp

list.o: list/list.cpp list/list.h
	g++ -Wall -c list/list.cpp -ggdb

viz.o: viz/viz.cpp viz/viz.h
	g++ -Wall -c viz/viz.cpp -lSDL -lSDL_image -lSDL_ttf

clean:
	rm *.o main
