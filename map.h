#ifndef __MAP_H__
#define __MAP_H__

namespace map {
    class Map;
}

#include <algorithm>
#include <cmath>
#include <set>
using namespace std;

#include "list/list.h"
#include "libs/clienttcp.h"
#include "libs/robotState.h"
#include "pathing/graph.h"
#include "pathing/pathing.h"
#include "viz/viz.h"

using namespace list;
using namespace pathing;

namespace map {

    const double eps = 1e-9;
	struct mp
	{
		double x, y; // pozycja
        List *ref;
        bool operator<(mp a) const {
            if (fabs(a.x-x) > eps) {
                return x < a.x;
            }
            return y < a.y;
        }
	};
	
    class Map {
        int indexMap, indexTemp;
		mp *mapTab;  //list of points we detected obstacles at, held as coordinatons relative to beggining of the map and pointer to aforementioned point in structure we held recognized walls.
        mp guard;
        Point *tempTab; //list of points we get from new scan, already transformed to relative coords to robot's current position
        RobotState rs; //robot's position and angle relative to map
        Client c;
        set<List*> walls; //walls we detected, held as lists of points
        pathing::Graph *G;
        pathing::Pathing *P;
        Viz *V;
        listRobot *measurement;

        public:
        Map(Client c_);
        void initMap();
        bool checkCollisionCircle(Point s, double r);
        bool checkCollisionRectangle(Point a, Point b, double r);
        double getScore(RobotState r);
        vector<pair<int,int> > get_positions(int size_x, int size_y);
        void update_location();
        void explore();

        private:
        void mergeMap();
        void getAbsolutePoints(RobotState rs, Point *result, int &indexTemp);
        void closePoints(Point a, mp **b);
    };
}

#endif
