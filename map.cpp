#include <algorithm>
#include <set>
#include <cmath>

#include "pathing/pathing.h"
#include "oldcode/mapa/mapa.h"
#include "libs/robotState.h"
#include "libs/MC.h"
#include "libs/clienttcp.h"

#define CLOSE_RANGE_SQUARED 9

using namespace std;
using namespace list;
using namespace pathing;

#include "map.h"

namespace map {

    Map :: Map(Client c_) : indexMap(0), c(c_) {
        rs = RobotState();
        mapTab = new mp[10000000];
        tempTab = new Point[100000];
        guard.x = -666;
    }

    void Map :: initMap(){
        V = new Viz(this);
        V -> start();
        rs.x = rs.y = 0.;
        rs.a = 0;
        measurement = c.look();
        getAbsolutePoints(rs, tempTab, indexTemp);
        mergeMap();
        V -> render(10);
        P = new Pathing(this);
        G = new Graph(Point(rs.x, rs.y), this, P);
        P->set_graph(G);
    }

    bool Map :: checkCollisionCircle(Point s, double r){
        for(int i = 0; i < indexMap; i++)
            if( hypot(mapTab[i].x-s.x, mapTab[i].y-s.y) < r ) return true;
        return false;
    }

    bool Map :: checkCollisionRectangle(Point a, Point b, double r){
        //fprintf(stderr,"check collision [%lf %lf], [%lf %lf], %lf\n", a.x, a.y, b.x, b.y, r);
        if(a < b) swap(a, b);
        if(a.y == b.y){
            for(int i = 0; i < indexMap; i++){
                if(mapTab[i].x > a.x && mapTab[i].x < b.x && mapTab[i].y > a.y - r && mapTab[i].y < a.y + r) return true;
            }
            return false;			
        }
        Point line1, line2, line3, line4; 
        line1.x = line2.x = (b.y-a.y)/(b.x-a.x);
        line3.x = line4.x = -(b.x-a.x)/(b.y-a.y);
        double x = sqrt(r/(1+line3.x*line3.x));
        line1.y = a.y + x * line3.x - line1.x * (a.x + x);
        line2.y = a.y - x * line3.x - line2.x * (a.x - x);
        line3.y = a.y - line3.x * a.x;
        line4.y = b.y - line4.x * b.x;
        //printf("lines:\ny1 = %lfx+%lf\ny2 = %lfx+%lf\ny3 = %lfx+%lf\ny4=%lfx+%lf\n", line1.x, line1.y, line2.x, line2.y, line3.x, line3.y, line4.x, line4.y);
        for(int i = 0; i < indexMap; i++){
            int a1 = line1.x * mapTab[i].x + line1.y > mapTab[i].y ? 1 : 0;
            int a2 = line2.x * mapTab[i].x + line2.y > mapTab[i].y ? 1 : 0;
            int a3 = line3.x * mapTab[i].x + line3.y > mapTab[i].y ? 1 : 0;
            int a4 = line4.x * mapTab[i].x + line4.y > mapTab[i].y ? 1 : 0;
            if ((a1+a2==1) && (a3+a4==1)) {
                //fprintf(stderr,"collide with [%lf %lf]\n", mapTab[i].x, mapTab[i].y);
                //printf("a = %d %d %d %d\n", a1, a2, a3, a4);
                return true;
            }
        }
        return false;			
    }


    void Map :: update_location() {
        measurement = c.look();
        printf("look ok\n");
        printf("rs before MC: %lf %lf %lf\n", rs.x, rs.y, rs.a);
        rs = MC(rs,*this);
        printf("rs after MC: %lf %lf %lf\n", rs.x, rs.y, rs.a);
        printf("MC ok\n");
    }

    void Map :: explore(){
        printf("explore:\n");
        P->seek_and_destroy(rs, c);
        printf("seek and destroy ok\n");
        update_location();
        getAbsolutePoints(rs, tempTab, indexTemp);
        printf("points ok\n");
        mergeMap();
        printf("merge map ok\n");
        V->render(10);
    }

    void Map :: mergeMap(){
        for(int i = 0; i < indexTemp; i++){
            mp *tmp[50];
            closePoints(tempTab[i], tmp);
            if(tmp[0]->x < -665.99){ //diaboł po przecynie
                mapTab[indexMap].ref = new List(tempTab[i]);
                walls.insert(mapTab[indexMap].ref);
            } else if (tmp[1]->x < -665.99){ //jeden punkt w otoczeniu
                if(tmp[0]->ref->is_first()){
                    mapTab[indexMap].ref = tmp[0]->ref->add_before(tempTab[i]);
                    walls.erase(walls.find(tmp[0]->ref));
                    walls.insert(mapTab[indexMap].ref);
                } else if(tmp[0]->ref->is_last()){
                    mapTab[indexMap].ref = tmp[0]->ref->add_behind(tempTab[i]);
                } else { //punkt jest wewnatrz jakiejs sciany, nie dopinamy sie w ogole
                    mapTab[indexMap].ref = new List(tempTab[i]);
                    walls.insert(mapTab[indexMap].ref);
                }
            } else {
                if(tmp[1]->ref->is_first() || tmp[1]->ref->is_last()){
                    if(tmp[0]->ref->is_first()){
                        mapTab[indexMap].ref = tmp[0]->ref->add_before(tempTab[i]);
                        if(tmp[1]->ref->is_first()){
                            walls.erase(walls.find(tmp[1]->ref));
                            tmp[1]->ref->reverse();
                            walls.insert(tmp[1]->ref->travel_first());
                        }
                        tmp[1]->ref->concat(tmp[0]->ref->travel_first());
                        walls.erase(walls.find(tmp[0]->ref));
                    } else if(tmp[0]->ref->is_last()){
                        mapTab[indexMap].ref = tmp[0]->ref->add_behind(tempTab[i]);;
                        if(tmp[1]->ref->is_last()){
                            walls.erase(walls.find(tmp[1]->ref->travel_first()));
                            tmp[1]->ref->reverse();
                        } else walls.erase(walls.find(tmp[1]->ref));
                        tmp[0]->ref->travel_last()->concat(tmp[1]->ref->travel_first());
                    } else {
                        mapTab[indexMap].ref = 	tmp[0]->ref->add_behind(tempTab[i]);
                    }
                } else { 
                    if(tmp[0]->ref->is_first()){
                        mapTab[indexMap].ref = tmp[0]->ref->add_before(tempTab[i]);
                        walls.erase(walls.find(tmp[0]->ref));
                        walls.insert(mapTab[indexMap].ref);
                    } else {
                        mapTab[indexMap].ref = tmp[0]->ref->add_behind(tempTab[i]);
                    }
                }
            }
            mapTab[indexMap].x = tempTab[i].x;
            mapTab[indexMap++].y = tempTab[i].y;
        }
        sort(mapTab, mapTab + indexMap);
        printf("map contains %d elements now\n", indexMap);
    }
    void Map :: getAbsolutePoints(RobotState rs, Point *result, int &indexTemp){
        listRobot *m = measurement;
        for(indexTemp = 0; measurement; measurement = measurement->next) {
            result[indexTemp++] = Point(rs.x + measurement->distance * cos(1.0*(-measurement->angle+rs.a)/180*M_PI), rs.y + measurement->distance * sin(1.0*(-measurement->angle+rs.a)/180*M_PI) );
            //fprintf(stderr,"angle = %d, distance = %d, point = %lf %lf\n", measurement->angle, measurement->distance, result[indexTemp-1].x, result[indexTemp-1].y);
        }
        measurement = m;
    }
    void Map :: closePoints(Point a, mp **b) {
        int j = 0;
        for(int i = 0; i < indexMap; i++)
            if( hypot(mapTab[i].x-a.x, mapTab[i].y-a.y) < CLOSE_RANGE_SQUARED )
                b[j++] = &mapTab[i];
        b[j++] = &guard;
        sort(b, b + j);
    }

    double Map :: getScore(RobotState r) {
        Point *temp = new Point[100000];
        int index = 0;
        double result = hypot(rs.x-r.x, rs.y-r.y) + (rs.a-r.a) * (rs.a-r.a);
        getAbsolutePoints(r, temp, index);
        int ins = 0;
        for (int i = 0; i < index; i++) {
            if (G->inside(temp[i])) {
                ins++;
                double dist = 1e9;
                for (int j = 0; j < indexMap; j++) {
                    double curd = hypot(temp[i].x-mapTab[j].x, temp[i].y-mapTab[j].y);
                    if (dist > curd) {
                        dist = curd;
                    }
                }
                result += dist * dist;
            }
        }
        delete temp;
        //fprintf(stderr, "result = %lf, inside: %d of %d\n", result, ins, index);
        return result;
    }

    vector<pair<int,int> > Map :: get_positions(int size_x, int size_y) {
        vector<pair<int,int> > res;
        double len_x = 0, len_y = 0;
        for (int i = 0; i < indexMap; i++) {
            len_x = max(len_x, fabs(rs.x-mapTab[i].x));
            len_y = max(len_y, fabs(rs.y-mapTab[i].y));
        }
        double scale = 2*max(len_x/size_x, len_y/size_y) + 0.01;
        len_x = 1.0 * size_x * scale;
        len_y = 1.0 * size_y * scale;
        res.push_back(make_pair(scale*100+0.9, 0));
        printf("scale = %lf cm / px, len = %lf %lf\n", scale, len_x, len_y);
        for (int i = 0; i < indexMap; i++) {
            if (fabs(rs.x-mapTab[i].x) < len_x && fabs(rs.y-mapTab[i].y) < len_y) {
                int x = (mapTab[i].x-rs.x) / scale + 0.5*size_x;
                int y = -(mapTab[i].y-rs.y) / scale + 0.5*size_y;
                res.push_back(make_pair(x,y));
            }
        }
        return res;
    }
};
