#ifndef _clienttcp_h
#define _clienttcp_h
#include "robotState.h"

class Client
{

public:
    void conn(char *hostname, int portno);
    int settask(char *msg);
    int disconnect();
    int go_forward(int val);
    int go_backward(int val);
    int rotate(int val);
    listRobot* look();
    int load(char* val);
private:
    int sockfd;
    
};

void error(const char *msg);


#endif
