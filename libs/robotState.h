#ifndef _robotState_h
#define _robotState_h

#include <algorithm>
using namespace std;

// zeby sie kompilowalo
struct RobotState
{
	double x,y; // pozycja
	double a; // kat patrzenia
    RobotState(double x_, double y_, double a_) : x(x_), y(y_), a(a_) {}
    RobotState() : x(0), y(0), a(0) {}
	RobotState operator+(RobotState p);
	RobotState getRandom();
	static RobotState randomSmall();
};

typedef struct element {
    struct element *next;
    int distance;
    int angle;
} listRobot;

double uniformRandom();

#endif
