#include <cstdio>
#include <algorithm>
#include <cmath>
#include "robotState.h"
#include "MC.h"
using namespace std;


// liczba prob MC
const int TRIES				= 3;
// liczba iteracji pojedynczej proby MC
const int ITERS				= 100;
// stala do normowania eksponenty przy liczeniu prawdopodobienstwa
const int EXP_NORM			= 1;

// Prawdopodobienstwo przejscia miedzy stanami
double getProb(double current, double next)
{
	if (next < current)
	{
		return 1.0;
	}
	double diff = next - current;
	diff = diff / EXP_NORM;
	return exp(-diff);
}

RobotState MC(RobotState start_position, map::Map &M)
{
	RobotState best = start_position;
	double value_best = M.getScore(best);

	for (int try_n = 0; try_n < TRIES; try_n++)
	{
		RobotState current = start_position.getRandom();
		double value_current = M.getScore(current);
		for (int iter = 0; iter < ITERS; iter++)
		{
			RobotState d = RobotState::randomSmall();
			RobotState next = current + d;
			double value_next = M.getScore(next);
			double probability = getProb(value_current, value_next);
			double rd = uniformRandom();
			if (rd < probability)
			{
				current = next;
				value_current = value_next;
				if (value_current < value_best)
				{
					value_best = value_current;
					best = current;
				}
			}
		}
	}
	return best;
}
