#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 
#include <iostream>
#include "robotState.h"

#include "clienttcp.h"

void parse(listRobot *first, char* data, int data_len)
{
    std::string s = data;
    if (s.size() > data_len)
    {
        s = s.substr(0, data_len);
    }
    if (data_len == 1) {
        first = NULL;
        return;
    }
    listRobot *elem; 
    elem = first; 
    int angle,distance;
    size_t pos = 0;
    std::string token;
    printf("s = %s\n", s.c_str());
    while ((pos = s.find("|")) != std::string::npos) {
        token = s.substr(0, pos);
        sscanf (token.c_str(), "%d:%d",&angle,&distance);
        //printf("token = %s, angle = %d, distance = %d\n", token.c_str(), angle, distance);
        //std::cout << "angle: " << angle<< " distance: " << distance  << std::endl;
        listRobot *nelem;
        nelem = (listRobot*) malloc (sizeof(listRobot));
        nelem->angle = angle;
        nelem->distance = distance;
        nelem->next = NULL;
        elem->next = nelem;

        elem = elem->next;
        
        s.erase(0, pos + 1);
    }   
    


}
void dump_list(listRobot *list)
{
    std::cout << "Dumping list:" << std::endl;
    listRobot *ptr=list;  
    if (ptr == NULL)
        printf("pointer is null\n");     

    while( ptr != NULL )          
    {
        std::cout << "angle: " << ptr->angle<< " distance: " << ptr->distance  << std::endl;
        ptr = ptr->next;            
    }                           
}

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

void Client::conn(char *hostname, int portno)
{

    struct sockaddr_in serv_addr;   
    struct hostent *server;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(hostname);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
      error("ERROR connecting");
}

int Client::go_forward(int val)
{
    char command[100] = "FORWARD:";
    char *tmp = new char[10];
    sprintf(tmp, "%d", val);
    strcat(command,tmp);
    settask(command);
    return 0;
}

int Client::go_backward(int val)
{
    char command[255] = "BACKWARD:";
    char *tmp = new char[10];
    sprintf(tmp, "%d", val);
    strcat(command,tmp);
    settask(command);
    return 0;
}
int Client::rotate(int val)
{
    char command[100] = "ROTATION:";
    char *tmp = new char[10];
    sprintf(tmp, "%d", val);
    strcat(command,tmp);
    settask(command);
    return 0;
}
int Client::load(char* val)
{
    char command[100] = "LOAD:";
    strcat(command,val);
    settask(command);
    return 0;
}
listRobot* Client::look()
{   
    int n;
    listRobot *first;
    first = (listRobot*) malloc (sizeof(listRobot));
    char buffer[5120];
    char msg[6] = "LOOK:";
    n = write(sockfd,msg,strlen(msg));
    if (n < 0) 
         error("ERROR writing to socket");
    n = read(sockfd,buffer,5120);
    printf("n = %d\n", n);
    if (n < 0) 
         error("ERROR reading from socket");
    parse(first,buffer,n);
    first = first->next;
    //dump_list(first);
    return first;
}

int Client::settask(char *msg)
{
    int n;
    char buffer[256];
    n = write(sockfd,msg,strlen(msg));
    if (n < 0) 
         error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
    return 0;
}
int Client::disconnect()
{
    close(sockfd);
    return 0;
}
