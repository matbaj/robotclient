#ifndef __MC_H__
#define __MC_H__

#include "../map.h"
#include "robotState.h"
RobotState MC(RobotState p, map::Map &M);

#endif
