#include <cmath>

#include "robotState.h"

// 1/EPS_NORM - max wychylenie epsilona
const int EPS_NORM			= 2;


// zwraca losowa liczbe z [0,1]
double uniformRandom()
{
	return 1.0 * rand() / RAND_MAX;
}

RobotState RobotState :: operator+(RobotState r) {
    return RobotState(x+r.x, y+r.y, a+r.a);
}

// Losowy punkt z okolicy p
RobotState RobotState :: getRandom()
{
	return *this + randomSmall();
}

// Losowe epsilonowe przemieszczenie
RobotState RobotState :: randomSmall()
{
	RobotState ret;
	ret.x = uniformRandom() / EPS_NORM;
	ret.y = uniformRandom() / EPS_NORM;
	ret.a = uniformRandom() * 2 * M_PI;
	return ret;
}

