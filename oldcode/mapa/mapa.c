#include<stdlib.h>
#include<math.h>
#include"mapa.h"

int space;	// aktualna pojemność tablicy wyników
int size;	// aktualna liczba punktów szczególnych
int pos;	// aktualna pozycja w tablicy punktów
int init;
char mask;	// maska nieodrzuconych punktów w tab[]
struct point tab[8];
struct point *wynik;


void add_wynik_point(struct point p){ // dorzuca punkt szczególny do tablicy wyników
	if(size+1>space){	// prosta implementacja wektora
		struct point *tmp;
		space++;
		space<<=1;
		tmp = malloc(space*sizeof(struct point)+8);
		for(int i=0;i<size;i++)
			tmp[i]=wynik[i];
		free(wynik);
		wynik=tmp;
	}
	wynik[size++]=p;
}

double find_min_x(char *m){	// znajduje minimum pośród niewybranych puntów
	double min = INFINITY;
	int pos;
	for(int i=0;i<8;i++){
		if(!(*m&(1<<i)))
			if(min>tab[i].x){
				min=tab[i].x;
				pos=i;
			}
	}
	*m|=1<<pos;
	return min;
}
double find_min_y(char *m){	// przepraszam, ale nie chciało mi się już pisać kolejnych if'ów
	double min = INFINITY;
	int pos;
	for(int i=0;i<8;i++){
		if(!(*m&(1<<i)))
			if(min>tab[i].y){
				min=tab[i].y;
				pos=i;
			}
	}
	*m|=1<<pos;
	return min;
}

struct point get_med(){	// zwraca medianę punktów
	char m = 0; // maska aktualnie niewybranych puntów
	struct point wynik;
	for(int i=0;i<4;i++)
		wynik.x=find_min_x(&m);
	m=0;
	for(int i=0;i<4;i++)
		wynik.y=find_min_y(&m);

	return wynik;
}

double dist(struct point a, struct point b){ // zwraca odległość pomiędzy dwoma punktami
	return hypot(a.x-b.x,a.y-b.y);
}

void remove_big(struct point p, char *m){	// usuwa najdalszy od p, niezamaskowany przez m punt
	double maxval=0;
	int max;
	for(int i=0;i<8;i++){
		if(!(*m&(1<<i)))
			if(dist(p,tab[i])>maxval){
				maxval=dist(p,tab[i]);
				max=i;
			}
	}
	*m|=1<<max;
}

struct point avg(struct point a, struct point b){ // zwraca punkt pomiędzy a i b
	struct point w;
	w.x=(a.x+b.x)/2.0;
	w.y=(a.y+b.y)/2.0;
	return w;
}

double angle(struct point v1, struct point v2){ // zwraca kąt pomiędzy węktorem v1 a v2
	return (v1.x*v2.x+v1.y*v2.y)/(hypot(v1.x,v1.y)*hypot(v2.x,v2.y));
}


void calculate(char m){	// właściwa funkcja obliczająca, kiedyś miała być interpolacja wielomianowa i liczenie pochodnych, skończyło się na liczeniu kątów między uśrednionymi punktami.
	struct point tab2[5];
	int k=0;
	double ang;
	for(int i=(init)&7;i!=((init-1)&7);i=(i+1)&7){
		if(!(m&(1<<i)))
			tab2[k++]=tab[i];
	}

	for(int i=0;i<5;i++);
	
	struct point a1,a2,a3;
	a1 = avg(tab2[0],tab2[1]);
	a2 = tab2[2];
	a3 = avg(tab2[3],tab2[4]);
	a1.x-=a2.x;
	a1.y-=a2.y;
	a3.x-=a2.x;
	a3.y-=a2.y;

	ang = angle(a1,a3);

	if(abs(ang)<0.8)
		add_wynik_point(a2);
}

	

void add_point(struct point p){	
	char m;
	tab[(init++)&7]=p;
	if(init>=8){
		struct point w;
		w=get_med();
		m=0;
		for(int i=0;i<3;i++){
			remove_big(w,&m);
		}

		calculate(m);
	}
}

struct point* get_point_list(){	
	return wynik;
}

int get_point_list_size(){
	return size-1;
}


