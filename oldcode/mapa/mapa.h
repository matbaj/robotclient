#ifndef _mapa_h
#define _mapa_h

/*
   Prosta biblioteka wyznaczająca Punkty Szczególne.
   */


struct point{
	double x;
	double y;
};

void add_point(struct point p);		// tu wchodzą bezpośrednio punkty z czujnika

struct point* get_point_list();		// zwraca wskaźnik na początek tablicy punktór
int get_point_list_size();		// zwraca rozmiar listy punktów

#endif
